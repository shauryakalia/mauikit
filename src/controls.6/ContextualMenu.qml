import QtQuick.Controls

Menu
{
    id: control
        
    function show(x, y, parent)
    {
        if (control.responsive)
        {
            control.open()
        }
        else
        {
            control.popup(parent,x ,y)
        }
    }    
}

