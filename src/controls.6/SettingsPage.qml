import org.mauikit.controls as Maui

Maui.ScrollColumn
{
    id: control
    spacing: Maui.Style.defaultSpacing*2
    
    property string title : i18nd("mauikit", "Settings")
}
